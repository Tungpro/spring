package com.example.t4.controllers.category;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.transaction.annotation.Transactional;

import com.example.t4.models.Category;
import com.example.t4.repository.CategoryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;

@Controller
@Transactional
public class CategoryContrller {

    @Autowired
    private CategoryRepository category;

    @GetMapping("category")
    public String index(Model model) {
        model.addAttribute("data", category.findAll());
        return "category/index";
    }

    @GetMapping("category/add")
    public String add() {
        return "category/add";
    }

    @PostMapping("category/add")
    public String create(@ModelAttribute("category-add") Category data) {
        System.out.println(data);
        return "redirect:/category";
    }
}
