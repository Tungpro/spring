<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>

    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>
 <div class="col-md-4">
     <div>
        <h2>Danh sách danh mục</h2>
        <a href="category/add">Add</a>
     </div>
    <table class="table" width="500">
        <thead>
            <tr>
                <th width="50">Stt</th>
                <th>Ten</th>
                <th width="50">Sửa</th>
                <th width="50">Xoa</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${data}" var="item">
            <tr>
                <td>1</td>
                <td>${item.name}</td>
                <td><button class="btn btn-primary"><a href="">Sửa</a></button></td>
                <td><button class="btn btn-danger"><a href="">Xóa</a></button></td>
            </tr>
            </c:forEach>
        </tbody>
    </table>    
 </div>
</body>
</html>
