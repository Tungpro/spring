package com.example.vuthanhtung.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import java.util.Collection;
import java.util.List;

import javax.persistence.Column;

@Entity
@Table(name = "categoies")
public class Category {
    @Id
    private String id;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @OneToMany(mappedBy = "category")
    private Collection<Product> product;

}
