package com.example.vuthanhtung.controllers;

import com.example.vuthanhtung.repositories.CategoryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Transactional
public class CategoryController {

    @Autowired
    private CategoryRepository categoryRepository;

    @GetMapping(value = "/category")
    public String viewCategories(Model model) {
        model.addAttribute("categories", categoryRepository.findAll());
        return "category/category";
    }

}
