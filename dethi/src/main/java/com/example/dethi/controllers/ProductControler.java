package com.example.dethi.controllers;

import com.example.dethi.repositories.ProductRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Transactional
public class ProductControler {

    @Autowired
    private ProductRepository productRepository;

    @GetMapping(value = "product")
    public String categoryProduct(Model model) {
        model.addAttribute("product", productRepository.findAll());
        System.out.println("tttttttttttttttttttttttttt");
        return "product/productList";
    }

}
