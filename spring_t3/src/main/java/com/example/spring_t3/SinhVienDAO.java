package com.example.spring_t3;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SinhVienDAO extends JpaRepository<SinhVien, String> {
}
