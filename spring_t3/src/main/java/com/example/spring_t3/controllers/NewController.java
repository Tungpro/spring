package com.example.spring_t3.controllers;

import com.example.spring_t3.Models.New;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NewController {

    @PostMapping(value = "/new")
    public New createNews(@RequestBody New model) {
        System.out.println("111111111111111");
        return model;
    }

    @PutMapping(value = "/new")
    public New getNews(@RequestBody New model) {
        return model;
    }
}
