package com.example.spring_t3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
// can co @Transactional de cac chuc nang insert/update/remove duoc bao boc
// trong Transaction
@Transactional
public class SinhVienController {
    @Autowired
    private SinhVienDAO sinhVienDAO;

    @GetMapping("/")
    public String home(Model model) {
        model.addAttribute("sinhViens", sinhVienDAO.findAll());

        // ve trang index.jsp hien thi all sinh vien
        System.out.println("44444444444444444444444444444444444");
        System.out.println(sinhVienDAO.findAll());
        return "index"; // /WEB-INF/page_views/index.jsp
    }

    @GetMapping("xoa")
    public String xoa(@RequestParam(name = "roll") String roll) {
        sinhVienDAO.deleteById(roll);

        // insert/update/remove thi quay ve index.jsp de show all = url "/" ben tren
        return "redirect:/";
    }

    @GetMapping("new-page")
    public String newPage(Model model) {
        model.addAttribute("sv", new SinhVien());

        return "new"; // /WEB-INF/page_views/new.jsp
    }

    @PostMapping("addnew")
    public String addnew(@ModelAttribute("sv") SinhVien sv) {
        System.out.println(sv);
        sinhVienDAO.save(sv);

        // insert/update/remove thi quay ve index.jsp de show all = url "/" ben tren
        return "redirect:/";
    }

    @GetMapping("chuan-bi-update")
    public String editPage(@RequestParam(name = "roll") String roll, Model model) {
        SinhVien sinhVien = sinhVienDAO.findById(roll).get();

        model.addAttribute("sv", sinhVien);

        return "edit"; // /WEB-INF/page_views/edit.jsp
    }

    @PostMapping("update")
    public String update(@ModelAttribute("sv") SinhVien sv) {
        sinhVienDAO.save(sv);

        // insert/update/remove thi quay ve index.jsp de show all = url "/" ben tren
        return "redirect:/";
    }

}
