<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
</head>
<body>
    <a href="/">Cancel</a>
    <form:form action="addnew" modelAttribute="sv">
        <form:label path="rollNo">Roll No</form:label>
        <form:input path="rollNo"/><br/>

        <form:label path="name">FullName</form:label>
        <form:input path="name"/><br/>

        <form:label path="className">Class Name</form:label>
        <form:input path="className"/><br/>

        <input type="submit"/>
    </form:form>
</body>
</html>
