<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
</head>
<body>
<a href="new-page">Add new</a>
<table>
    <tr>
        <th>RollNo</th>
        <th>Name</th>
        <th>Class</th>
        <th>Edit</th>
        <th>Xoa</th>
    </tr>
    <c:forEach items="${sinhViens}" var="svvv">
        <tr>
            <td>${svvv.rollNo}</td>
            <td>${svvv.name}</td>
            <td>${svvv.className}</td>
            <td>
                <a href="chuan-bi-update?roll=${svvv.rollNo}">Edit</a>
            </td>
            <td>
                <a href="xoa?roll=${svvv.rollNo}">Xoa</a>
            </td>
        </tr>
    </c:forEach>
</table>

</body>
</html>
