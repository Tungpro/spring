create table t_student(roll_no varchar(50) primary key, full_name varchar(50), class_name varchar(50));
insert into t_student(roll_no, full_name, class_name) values
    ('S0011', 'Ho Van Cuong', 'C0909KV'),
    ('S0022', 'Ho Thanh Tung', 'C0911LV');
select * from t_student;
